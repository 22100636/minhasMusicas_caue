//
//  ViewController.swift
//  minhasMusicas_caue
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

struct Musica {
    
    let nomeMusica: String
    let nomeAlbum : String
    let nomecantor : String
    let imgPequena : String
    let imgGrande : String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var listaDeMusicas:[Musica] = []
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.listaDeMusicas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "minhaCelula", for: indexPath) as! MyCell
        
        let musicas = self.listaDeMusicas[indexPath.row]
        
        cell.musica.text = musicas.nomeMusica
        cell.album.text = musicas.nomeAlbum
        cell.cantor.text = musicas.nomecantor
        cell.capa.image = UIImage(named: musicas.imgPequena)
        
        return cell
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        
        self.tableView.delegate = self
        
        self.listaDeMusicas.append(Musica(nomeMusica: "Pontos Cardeais", nomeAlbum: "Album Vivo!", nomecantor: "Alceu Valenca", imgPequena: "capa_alceu_pequeno", imgGrande: "capa_alceu_grande"))
        
        self.listaDeMusicas.append(Musica(nomeMusica: "Menor Abandonado", nomeAlbum: "Album Patota de Cosmo", nomecantor: "Zeca Pagodinho", imgPequena: "capa_zeca_pequeno", imgGrande: "capa_zeca_grande"))
        
        self.listaDeMusicas.append(Musica(nomeMusica: "Tiro ao Álvaro", nomeAlbum: "Album Adoniran Barbosa e convidados", nomecantor: "Adoniran Barbosa", imgPequena: "capa_adoniran_pequeno", imgGrande: "capa_adhoniran_grande"))
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detalhesMusica = segue.destination as!  DetalhesMusica
        let indice = sender as! Int
        let musica = self.listaDeMusicas[indice]
        
        detalhesMusica.nomeimagem = musica.imgGrande
        detalhesMusica.nomemusica = musica.nomeMusica
        detalhesMusica.nomealbum = musica.nomeAlbum
        detalhesMusica.nomecantor = musica.nomecantor
    }


}

