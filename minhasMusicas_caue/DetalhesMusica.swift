//
//  DetalhesMusica.swift
//  minhasMusicas_caue
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

class DetalhesMusica: UIViewController {
    
    var nomeimagem: String = ""
    var nomemusica: String = ""
    var nomealbum: String = ""
    var nomecantor: String = ""

    @IBOutlet weak var capa: UIImageView!
    @IBOutlet weak var musica: UILabel!
    @IBOutlet weak var album: UILabel!
    @IBOutlet weak var cantor: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.capa.image = UIImage(named: self.nomeimagem)
        self.musica.text = self.nomemusica
        self.cantor.text = self.nomecantor
        self.album.text = self.nomealbum
     

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
